//
//  SecondViewController.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

import UIKit
class SecondViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{
    let pageControl = UIPageControl()
    lazy var VCar: [UIViewController] = {
        return [self.VCInstance(name: "Acela"),self.VCInstance(name: "Duo"),self.VCInstance(name: "Mono"),self.VCInstance(name: "Perchloric"),self.VCInstance(name: "Radiosotope"),self.VCInstance(name: "AcidDigestion"),self.VCInstance(name: "AcelaM"),self.VCInstance(name: "FloorMounted"),self.VCInstance(name: "PPH")]
    }()
    private func VCInstance(name: String)->UIViewController{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
       
        if let Acela = VCar.first{
            setViewControllers([Acela], direction: .forward, animated: false, completion: nil)
            
        }
    }
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        guard let viewControllerIndex = VCar.index(of: viewController) else{
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else{
            return nil
        }
        
        guard VCar.count > previousIndex else{
            return nil
        }
        
        return VCar[previousIndex]
    } 

    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        guard let viewControllerIndex = VCar.index(of: viewController) else{
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < VCar.count  else{
            return nil
        }
        
        guard VCar.count > nextIndex else{
            return nil
        }
        return VCar[nextIndex]
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews{
            if view is UIScrollView{
                view.frame = UIScreen.main.bounds
            }else if view is UIPageControl{
                view.backgroundColor = UIColor.clear
            }
        }
    }
    func nextPageWithIndex(index: Int){
        let nextWalkthroughVC = VCar[index]
        setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
    }
    func previousPageWithIndex(index: Int){
        let nextWalkthroughVC = VCar[index]
        setViewControllers([nextWalkthroughVC], direction: .reverse, animated: true, completion: nil)
    }
    
    /**
     public func presentationCount(for pageViewController: UIPageViewController) -> Int{
        return VCar.count
    }
     public func presentationIndex(for pageViewController: UIPageViewController) -> Int{
        guard let AcelaViewController  = viewControllers?.first, let firstViewControllerIndex = VCar.index(of: AcelaViewController)else{
            return 0
        }
        return firstViewControllerIndex
    }
    **/

}
