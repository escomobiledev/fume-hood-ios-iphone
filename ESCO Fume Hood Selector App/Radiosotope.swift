//
//  Duo.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

//
//  Acela.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

import UIKit
class Radiosotope: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    @IBOutlet weak var txtStaticPressure: UILabel!
    @IBOutlet weak var txtExhaustVolume: UILabel!
    @IBOutlet weak var txtExhaustDiameter: UILabel!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var tfSelectCode: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    var num: String!
    var list = ["EFI-4UD4CW-8","EFI-4UD6CW-8","EFI-4UD4VW-8","EFI-4UD6VW-8","EFI-4UD4CW-9","EFI-4UD6CW-9","EFI-4UD4VW-9","EFI-4UD6VW-9","EFI-5UD4CW-8","EFI-5UD6CW-8","EFI-5UD4VW-8","EFI-5UD6VW-8","EFI-5UD4CW-9","EFI-5UD6CW-9","EFI-5UD4VW-9","EFI-5UD6VW-9","EFI-6UD4CW-8","EFI-6UD6CW-8","EFI-6UD4VW-8","EFI-6UD6VW-8","EFI-6UD4CW-9","EFI-6UD6CW-9","EFI-6UD4VW-9","EFI-6UD6VW-9","EFI-8UD4VW-8","EFI-8UD6VW-8","EFI-8UD4VW-9","EFI-8UD6VW-9"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSelectCode.textAlignment = .center
        self.btnCalculate.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.backgroundColor = UIColor.clear;
        self.tfSelectCode.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.layer.borderWidth = 1.0
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = list.count
        if pickerView==pickerView{
            countrows = self.list.count
        }
        return countrows
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerView){
            let titleRow = list[row]
            return titleRow
        }else{
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView{
            let select = list[row]
            if (row<=7){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1070cmh"
                self.txtStaticPressure.text = "36Pa"
                self.tfSelectCode.text = select
                num = "1"
            }else if(row>7&&row<=15){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1391cmh"
                self.txtStaticPressure.text = "47Pa"
                self.tfSelectCode.text = select
                num = "2"
            }else if(row>15&&row<=23){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1712cmh"
                self.txtStaticPressure.text = "61Pa"
                self.tfSelectCode.text = select
                num = "3"
            }else if(row>23&&row<=27){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "2354cmh"
                self.txtStaticPressure.text = "40Pa"
                self.tfSelectCode.text = select
                num = "4"
            }
            self.pickerView.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tfSelectCode{
            self.pickerView.isHidden = false
            textField.endEditing(true)
        }
    }
    @IBAction func btnCalculate(_ sender: Any) {
        let x = self.tfSelectCode.text
        if(String(describing: x)=="Optional(\"SELECT CODE\")"){
            let alert = UIAlertController(title: "", message: "Please select model code to proceed", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let viewController = storyboard.instantiateViewController(withIdentifier :"Calculate")
            //self.present(viewController, animated: true)
            performSegue(withIdentifier: "RadiosotopeCalculate", sender: "Radiosotope")
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let parent = self.parent as! SecondViewController
        parent.nextPageWithIndex(index: 5)
    }
    
    @IBAction func btnPrev(_ sender: Any) {
        let parent = self.parent as! SecondViewController
        parent.previousPageWithIndex(index: 3)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RadiosotopeCalculate"{
            if let destination = segue.destination as? Calculate{
                destination.hoodType = sender as? String
                destination.num = num
            }
        }
    }
}

