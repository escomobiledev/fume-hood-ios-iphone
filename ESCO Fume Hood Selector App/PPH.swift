//
//  Duo.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

//
//  Acela.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

import UIKit
class PPH: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    @IBOutlet weak var txtStaticPressure: UILabel!
    @IBOutlet weak var txtExhaustVolume: UILabel!
    @IBOutlet weak var txtExhaustDiameter: UILabel!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var tfSelectCode: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    var num: String!
    var list = ["PPH-4UDPVW-8","PPH-4UDPCW-8","PPH-4UDPVW-9","PPH-4UDPCW-9","PPH-5UDPVW-8","PPH-5UDPCW-8","PPH-5UDPVW-9","PPH-5UDPCW-9","PPH-6UDPVW-8","PPH-6UDPCW-8","PPH-6UDPVW-9","PPH-6UDPCW-9","PPH-8UDPVW-8","PPH-8UDPCW-8","PPH-8UDPVW-9","PPH-8UDPCW-9"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSelectCode.textAlignment = .center
        self.btnCalculate.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.backgroundColor = UIColor.clear;
        self.tfSelectCode.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.layer.borderWidth = 1.0
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = list.count
        if pickerView==pickerView{
            countrows = self.list.count
        }
        return countrows
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerView){
            let titleRow = list[row]
            return titleRow
        }else{
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView{
            let select = list[row]
            if (row<=3){
                self.txtExhaustDiameter.text = "250mm"
                self.txtExhaustVolume.text = "1305cmh"
                self.txtStaticPressure.text = "73Pa"
                self.tfSelectCode.text = select
                num = "1"
            }else if(row>3&&row<=7){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1705cmh"
                self.txtStaticPressure.text = "95Pa"
                self.tfSelectCode.text = select
                num = "2"
            }else if(row>7&&row<=11){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "2105cmh"
                self.txtStaticPressure.text = "117Pa"
                self.tfSelectCode.text = select
                num = "3"
            }else if(row>11&&row<=15){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "2904cmh"
                self.txtStaticPressure.text = "135Pa"
                self.tfSelectCode.text = select
                num = "4"
            }
            self.pickerView.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tfSelectCode{
            self.pickerView.isHidden = false
            textField.endEditing(true)
        }
    }
    @IBAction func btnCalculate(_ sender: Any) {
        let x = self.tfSelectCode.text
        if(String(describing: x)=="Optional(\"SELECT CODE\")"){
            let alert = UIAlertController(title: "", message: "Please select model code to proceed", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let viewController = storyboard.instantiateViewController(withIdentifier :"Calculate")
            //self.present(viewController, animated: true)
            performSegue(withIdentifier: "PPHCalculate", sender: "PPH")
        }
    }

    
    
    @IBAction func btnPrev(_ sender: Any) {
        let parent = self.parent as! SecondViewController
        parent.previousPageWithIndex(index: 7)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PPHCalculate"{
            if let destination = segue.destination as? Calculate{
                destination.hoodType = sender as? String
                destination.num = num
            }
        }
    }
}

