//
//  Duo.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

//
//  Acela.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

import UIKit
class AcidDigestion: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    @IBOutlet weak var txtStaticPressure: UILabel!
    @IBOutlet weak var txtExhaustVolume: UILabel!
    @IBOutlet weak var txtExhaustDiameter: UILabel!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var tfSelectCode: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    var num: String!
    var list = ["EFQ-4UDCCW-8","EFQ-4UDPCW-8","EFQ-4UDCVW-8","EFQ-4UDPVW-8","EFQ-4UDCCW-9","EFQ-4UDPCW-9","EFQ-4UDCVW-9","EFQ-4UDPVW-9","EFQ-5UDCCW-8","EFQ-5UDPCW-8","EFQ-5UDCVW-8","EFQ-5UDPVW-8","EFQ-5UDCCW-9","EFQ-5UDPCW-9","EFQ-5UDCVW-9","EFQ-5UDPVW-9","EFQ-6UDCCW-8","EFQ-6UDPCW-8","EFQ-6UDCVW-8","EFQ-6UDPVW-8","EFQ-6UDCCW-9","EFQ-6UDPCW-9","EFQ-6UDCVW-9","EFQ-6UDPVW-9","EFQ-8UDCVW-8","EFQ-8UDPVW-8","EFQ-8UDCVW-9","EFQ-8UDPVW-9"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSelectCode.textAlignment = .center
        self.btnCalculate.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.backgroundColor = UIColor.clear;
        self.tfSelectCode.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectCode.layer.borderWidth = 1.0
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = list.count
        if pickerView==pickerView{
            countrows = self.list.count
        }
        return countrows
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerView){
            let titleRow = list[row]
            return titleRow
        }else{
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView{
            let select = list[row]
            if (row<=8){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1070cmh"
                self.txtStaticPressure.text = "36Pa"
                self.tfSelectCode.text = select
                num = "1"
            }else if(row>8&&row<=16){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1391cmh"
                self.txtStaticPressure.text = "47Pa"
                self.tfSelectCode.text = select
                num = "2"
            }else if(row>16&&row<=24){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "1712cmh"
                self.txtStaticPressure.text = "61Pa"
                self.tfSelectCode.text = select
                num = "3"
            }else if(row>24&&row<=28){
                self.txtExhaustDiameter.text = "305mm"
                self.txtExhaustVolume.text = "2354cmh"
                self.txtStaticPressure.text = "40Pa"
                self.tfSelectCode.text = select
                num = "4"
            }
            self.pickerView.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tfSelectCode{
            self.pickerView.isHidden = false
            textField.endEditing(true)
        }
    }
    @IBAction func btnCalculate(_ sender: Any) {
        let x = self.tfSelectCode.text
        if(String(describing: x)=="Optional(\"SELECT CODE\")"){
            let alert = UIAlertController(title: "", message: "Please select model code to proceed", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let viewController = storyboard.instantiateViewController(withIdentifier :"Calculate")
            //self.present(viewController, animated: true)
            performSegue(withIdentifier: "AcidDigestionCalculate", sender: "AcidDigestion")
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let parent = self.parent as! SecondViewController
        parent.nextPageWithIndex(index: 6)
    }
    @IBAction func btnPrev(_ sender: Any) {
        let parent = self.parent as! SecondViewController
        parent.previousPageWithIndex(index: 4)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AcidDigestionCalculate"{
            if let destination = segue.destination as? Calculate{
                destination.hoodType = sender as? String
                destination.num = num
            }
        }
    }
}

