//
//  Calculate.swift
//  ESCO Fume Hood Selector App
//
//  Created by AndroidDev on 23/02/2017.
//  Copyright © 2017 ESCO Phil. Inc. All rights reserved.
//

import UIKit
class Calculate: UIViewController{
 
    @IBOutlet weak var lb1: UILabel!
    @IBOutlet weak var lb2: UILabel!
    @IBOutlet weak var lb3: UILabel!
    @IBOutlet weak var lb4: UILabel!
    @IBOutlet weak var lb5: UILabel!
    @IBOutlet weak var txtTotalPL: UILabel!
    @IBOutlet weak var txtPLDR: UILabel!
    @IBOutlet weak var txtPL45: UILabel!
    @IBOutlet weak var txtPL90: UILabel!
    @IBOutlet weak var txtPLCircular: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var tf45Elbow: UITextField!
    @IBOutlet weak var tf90Elbow: UITextField!
    @IBOutlet weak var tfDuctLength: UITextField!
    @IBOutlet weak var tfSelectDD: UITextField!
    @IBOutlet weak var tf250Reducer: UITextField!
    @IBOutlet weak var tf305Reducer: UITextField!
    @IBOutlet weak var ductDiameterPickerView: UIPickerView!
    var hoodType: String!
    var num: String!
    var num1 = 0.0226, num2 = 0.0248
    var finalTotalPressure: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.tf45Elbow.textAlignment = .center
        self.tf90Elbow.textAlignment = .center
        self.tfDuctLength.textAlignment = .center
        self.tfSelectDD.textAlignment = .center
        self.tf250Reducer.textAlignment = .center
        self.tf305Reducer.textAlignment = .center
        self.btnCalculate.layer.borderColor = UIColor.white.cgColor;
        self.btnClearAll.layer.borderColor = UIColor.white.cgColor;
        self.tf45Elbow.backgroundColor = UIColor.clear;
        self.tf90Elbow.backgroundColor = UIColor.clear;
        self.tfDuctLength.backgroundColor = UIColor.clear;
        self.tfSelectDD.backgroundColor = UIColor.clear;
        self.tf250Reducer.backgroundColor = UIColor.clear;
        self.tf305Reducer.backgroundColor = UIColor.clear;
        self.tf45Elbow.layer.borderColor = UIColor.white.cgColor;
        self.tf90Elbow.layer.borderColor = UIColor.white.cgColor;
        self.tfDuctLength.layer.borderColor = UIColor.white.cgColor;
        self.tfSelectDD.layer.borderColor = UIColor.white.cgColor;
        self.tf250Reducer.layer.borderColor = UIColor.white.cgColor;
        self.tf305Reducer.layer.borderColor = UIColor.white.cgColor;
        self.tf45Elbow.layer.borderWidth = 1.0
        self.tf90Elbow.layer.borderWidth = 1.0
        self.tfDuctLength.layer.borderWidth = 1.0
        self.tfSelectDD.layer.borderWidth = 1.0
        self.tf250Reducer.layer.borderWidth = 1.0
        self.tf305Reducer.layer.borderWidth = 1.0
        
        
        if(hoodType=="Acela"){
            imgHeader.image = UIImage(named: "acelacalculatetitle")
        }else if(hoodType=="Duo"){
            imgHeader.image = UIImage(named: "duocalculatetitle")
        }else if(hoodType=="Mono"){
            imgHeader.image = UIImage(named: "monocalculatetitle")
        }else if(hoodType=="Perchloric"){
            imgHeader.image = UIImage(named: "perchloriccalculatetitle")
        }else if(hoodType=="Radiosotope"){
            imgHeader.image = UIImage(named: "radiosotopecalculatetitle")
        }else if(hoodType=="AcidDigestion"){
            imgHeader.image = UIImage(named: "aciddigestioncalculatetitle")
        }else if(hoodType=="AcelaM"){
            imgHeader.image = UIImage(named: "acelamcalculatetitle")
        }else if(hoodType=="FloorMounted"){
            imgHeader.image = UIImage(named: "floormountedcalculatetitle")
        }else if(hoodType=="PPH"){
            imgHeader.image = UIImage(named: "pphcalculatetitle")
        }
     
    }
    let list = ["250","305"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var countrows : Int = list.count
        if pickerView==pickerView{
            countrows = self.list.count
        }
        return countrows
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerView){
            let titleRow = list[row]
            return titleRow
        }else{
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView{
            let select = list[row]
            self.tfSelectDD.text = select
            self.ductDiameterPickerView.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tfSelectDD{
            self.ductDiameterPickerView.isHidden = false
            textField.endEditing(true)
        }
    }

    
    
    @IBAction func btnCalculate(_ sender: Any) {
        if(self.tfSelectDD.text=="Select Duct Diameter Here"){
            let alert = UIAlertController(title: "", message: "Please select Duct Diameter", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            if(tfDuctLength.text?.isEmpty)!{
                tfDuctLength.text = "0.00"
            }; if(tf250Reducer.text?.isEmpty)!{
                tf250Reducer.text = "0.00"
            }; if(tf305Reducer.text?.isEmpty)!{
                tf305Reducer.text = "0.00"
            }; if(tf90Elbow.text?.isEmpty)!{
                tf90Elbow.text = "0.00"
            }; if(tf45Elbow.text?.isEmpty)!{
                tf45Elbow.text = "0.00"
            }
            switch self.hoodType {
            case "Acela":
                if(self.num == "1"){
                    let staticPressure = 66.6
                    if(self.tfSelectDD.text == "250"){
                        let circular = 3.14
                        let ninetydeg = 32.5
                        let fourtyfivedeg = 16.25
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }else if(self.tfSelectDD.text == "305"){
                        let circular = 1.28
                        let ninetydeg = 14.00
                        let fourtyfivedeg = 7.00
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                        
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }else if(self.num=="2"){
                    let staticPressure = 76.6
                    if(self.tfSelectDD.text == "250"){
                        let circular = 5.42
                        let ninetydeg = 42.45
                        let fourtyfivedeg = 21.23
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }else if(self.tfSelectDD.text == "305"){
                        let circular = 2.14
                        let ninetydeg = 23.9
                        let fourtyfivedeg = 11.95
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }else if(self.num=="3"){
                    let staticPressure = 94.4
                    if(self.tfSelectDD.text == "250"){
                        let circular = 6.42
                        let ninetydeg = 47.63
                        let fourtyfivedeg = 23.82
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }else if(self.tfSelectDD.text == "305"){
                        let circular = 2.70
                        let ninetydeg = 29.5
                        let fourtyfivedeg = 14.75
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }else if(self.num=="4"){
                    let staticPressure = 74.3
                    if(self.tfSelectDD.text == "250"){
                        let circular = 9.75
                        let ninetydeg = 72.30
                        let fourtyfivedeg = 36.15
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }else if(self.tfSelectDD.text == "305"){
                        let circular = 4.09
                        let ninetydeg = 44.78
                        let fourtyfivedeg = 22.39
                        self.txtPLCircular.isHidden = false
                        self.txtPL90.isHidden = false
                        self.txtPL45.isHidden = false
                        self.txtPLDR.isHidden = false
                        self.txtTotalPL.isHidden = false
                        if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                            if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else{
                                self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                self.txtTotalPL.text = finalTotalPressure.toString()
                            }
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                if(finalTotalPressure==nil){
                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    if(finalTotalPressure>=1&&finalTotalPressure<=300){
                        let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.lb1.isHidden = false
                        self.lb2.isHidden = false
                        self.lb3.isHidden = false
                        self.lb1.text = "The Recommended fan is PP025"
                        self.lb2.text = "Note: 305mm - 250mm and "
                        self.lb3.text = "250mm - 200mm Reducer is required. "
                    }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                        let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.lb1.isHidden = false
                        self.lb2.isHidden = false
                        self.lb1.text = "The Recommended fan is PP030"
                        self.lb2.text = "Note: No Reducer is required"
                    }else if(finalTotalPressure>=501){
                        let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.lb1.isHidden = false
                        self.lb2.isHidden = false
                        self.lb3.isHidden = false
                        self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                        self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                        self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                    }
                }
                
            
                break
                case "Duo":
                    if(self.num == "1"){
                        let staticPressure = 70.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.49
                            let ninetydeg = 24.375
                            let fourtyfivedeg = 12.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.0
                            let ninetydeg = 10.0
                            let fourtyfivedeg = 5.0
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 75.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.49
                            let ninetydeg = 24.375
                            let fourtyfivedeg = 12.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.0
                            let ninetydeg = 10.0
                            let fourtyfivedeg = 5.0
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 80.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 3.35
                            let ninetydeg = 34.38
                            let fourtyfivedeg = 17.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.42
                            let ninetydeg = 14.5
                            let fourtyfivedeg = 7.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 86.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 3.35
                            let ninetydeg = 34.38
                            let fourtyfivedeg = 17.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.42
                            let ninetydeg = 14.5
                            let fourtyfivedeg = 7.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "Mono":
                    if(self.num == "1"){
                        let staticPressure = 68.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 1.9
                            let ninetydeg = 17.5
                            let fourtyfivedeg = 8.75
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 0.7
                            let ninetydeg = 7.5
                            let fourtyfivedeg = 3.75
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 80.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.98
                            let ninetydeg = 29.0
                            let fourtyfivedeg = 14.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.14
                            let ninetydeg = 12.5
                            let fourtyfivedeg = 6.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 88.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 4.14
                            let ninetydeg = 42.5
                            let fourtyfivedeg = 21.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.7
                            let ninetydeg = 18.0
                            let fourtyfivedeg = 9.0
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "Perchloric":
                    if(self.num == "1"){
                        let staticPressure = 36.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 1.7
                            let ninetydeg = 16.875
                            let fourtyfivedeg = 8.84
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 0.7
                            let ninetydeg = 7.0
                            let fourtyfivedeg = 3.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 47.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.7
                            let ninetydeg =  25.5
                            let fourtyfivedeg = 13.75
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.14
                            let ninetydeg = 11.67
                            let fourtyfivedeg = 5.83
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 80.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 3.35
                            let ninetydeg = 34.38
                            let fourtyfivedeg = 17.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.42
                            let ninetydeg = 14.5
                            let fourtyfivedeg = 7.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 40.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.0
                            let ninetydeg = 57.59
                            let fourtyfivedeg = 28.80
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 3.14
                            let ninetydeg = 33.85
                            let fourtyfivedeg = 16.925
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                    
                    
                    break
                case "Radiosotope":
                    if(self.num == "1"){
                        let staticPressure = 36.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 1.7
                            let ninetydeg = 16.875
                            let fourtyfivedeg = 8.84
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 0.7
                            let ninetydeg = 7.0
                            let fourtyfivedeg = 3.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 47.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.7
                            let ninetydeg =  27.5
                            let fourtyfivedeg = 13.75
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.14
                            let ninetydeg = 11.67
                            let fourtyfivedeg = 5.83
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 61.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 4.07
                            let ninetydeg = 41.88
                            let fourtyfivedeg = 20.94
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.7
                            let ninetydeg = 18.025
                            let fourtyfivedeg = 9.01
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 40.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.0
                            let ninetydeg = 57.59
                            let fourtyfivedeg = 28.80
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 3.14
                            let ninetydeg = 33.85
                            let fourtyfivedeg = 16.925
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "AcidDigestion":
                    if(self.num == "1"){
                        let staticPressure = 36.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 1.7
                            let ninetydeg = 16.875
                            let fourtyfivedeg = 8.84
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 0.7
                            let ninetydeg = 7.0
                            let fourtyfivedeg = 3.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 47.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 2.7
                            let ninetydeg =  27.5
                            let fourtyfivedeg = 13.75
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.14
                            let ninetydeg = 11.67
                            let fourtyfivedeg = 5.83
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 61.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 4.07
                            let ninetydeg = 41.88
                            let fourtyfivedeg = 20.94
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.7
                            let ninetydeg = 18.025
                            let fourtyfivedeg = 9.01
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 40.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.0
                            let ninetydeg = 57.59
                            let fourtyfivedeg = 28.80
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 3.14
                            let ninetydeg = 33.85
                            let fourtyfivedeg = 16.925
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "AcelaM":
                    if(self.num == "1"){
                        let staticPressure = 66.6
                        if(self.tfSelectDD.text == "250"){
                            let circular = 3.14
                            let ninetydeg = 31.88
                            let fourtyfivedeg = 15.94
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.28
                            let ninetydeg = 13.575
                            let fourtyfivedeg = 6.7875
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 76.6
                        if(self.tfSelectDD.text == "250"){
                            let circular = 3.14
                            let ninetydeg = 31.88
                            let fourtyfivedeg = 15.94
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.28
                            let ninetydeg = 13.575
                            let fourtyfivedeg = 6.7875
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 94.7
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.0
                            let ninetydeg = 46.71
                            let fourtyfivedeg = 23.36
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular =  2.7
                            let ninetydeg = 28.85
                            let fourtyfivedeg = 14.425
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 74.3
                        if(self.tfSelectDD.text == "250"){
                            let circular = 9.16
                            let ninetydeg = 71.31
                            let fourtyfivedeg = 35.66
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 4.122
                            let ninetydeg = 44.04
                            let fourtyfivedeg = 22.02
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "FloorMounted":
                    if(self.num == "1"){
                        let staticPressure = 31.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.56
                            let ninetydeg = 48.27
                            let fourtyfivedeg = 24.14
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 2.84
                            let ninetydeg = 31.1
                            let fourtyfivedeg = 15.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 52.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 8.53
                            let ninetydeg = 62.76
                            let fourtyfivedeg = 31.38
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 3.69
                            let ninetydeg = 40.43
                            let fourtyfivedeg = 20.22
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 80.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 10.5
                            let ninetydeg = 77.24
                            let fourtyfivedeg = 38.62
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular =  4.54
                            let ninetydeg = 49.77
                            let fourtyfivedeg = 24.88
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 38.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 14.44
                            let ninetydeg = 106.21
                            let fourtyfivedeg = 52.106
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 6.24
                            let ninetydeg = 68.43
                            let fourtyfivedeg = 34.22
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
                case "PPH":
                    if(self.num == "1"){
                        let staticPressure = 73.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 1.0
                            let ninetydeg = 10.625
                            let fourtyfivedeg = 5.31
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 2.56
                            let ninetydeg = 25.0
                            let fourtyfivedeg = 12.5
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="2"){
                        let staticPressure = 95.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 4.14
                            let ninetydeg = 42.5
                            let fourtyfivedeg = 21.25
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 1.7
                            let ninetydeg = 17.75
                            let fourtyfivedeg = 8.875
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="3"){
                        let staticPressure = 117.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 6.0
                            let ninetydeg = 52.47
                            let fourtyfivedeg = 26.24
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 2.56
                            let ninetydeg = 27.2
                            let fourtyfivedeg = 13.6
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }else if(self.num=="4"){
                        let staticPressure = 135.0
                        if(self.tfSelectDD.text == "250"){
                            let circular = 8.27
                            let ninetydeg = 72.39
                            let fourtyfivedeg = 36.19
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                //self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                        }else if(self.tfSelectDD.text == "305"){
                            let circular = 3.53
                            let ninetydeg = 37.52
                            let fourtyfivedeg = 18.76
                            self.txtPLCircular.isHidden = false
                            self.txtPL90.isHidden = false
                            self.txtPL45.isHidden = false
                            self.txtPLDR.isHidden = false
                            self.txtTotalPL.isHidden = false
                            if((tfDuctLength.text?.isNumber)! || (tf250Reducer.text?.isNumber)! || (tf305Reducer.text?.isNumber)! || (tf90Elbow.text?.isNumber)! || (tf45Elbow.text?.isNumber)!){
                                if(tfDuctLength.text?.toDouble()==nil || tf250Reducer.text?.toDouble()==nil || tf305Reducer.text?.toDouble()==nil || tf90Elbow.text?.toDouble()==nil || tf45Elbow.text?.toDouble()==nil){
                                    let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    self.txtPLCircular.text =  ((self.tfDuctLength.text?.toDouble())! * circular).toString()+" Pa"
                                    self.txtPL90.text = ((self.tf90Elbow.text?.toDouble())! * ninetydeg).toString()+" Pa"
                                    self.txtPL45.text = ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg).toString()+" Pa"
                                    self.txtPLDR.text = (((self.tf250Reducer.text?.toDouble())!*num1) + ((self.tf305Reducer.text?.toDouble())!)*num2).toString()+" Pa"
                                    finalTotalPressure = (((self.tfDuctLength.text?.toDouble())! * circular) + ((self.tf45Elbow.text?.toDouble())! * fourtyfivedeg)+((self.tf90Elbow.text?.toDouble())! * ninetydeg)+(((self.tf250Reducer.text?.toDouble())! * num1)+((self.tf305Reducer.text?.toDouble())! * num2)+staticPressure))
                                    self.txtTotalPL.text = finalTotalPressure.toString()
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    if(finalTotalPressure==nil){
                        let alert = UIAlertController(title: "", message: "Please enter a valid number", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        if(finalTotalPressure>=1&&finalTotalPressure<=300){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP025\nNote: 305mm - 250mm and 250mm - 200mm Reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "The Recommended fan is PP025"
                            self.lb2.text = "Note: 305mm - 250mm and "
                            self.lb3.text = "250mm - 200mm Reducer is required. "
                        }else if(finalTotalPressure>=301&&finalTotalPressure<=500){
                            let alert = UIAlertController(title: "", message: "The Recommended fan is PP030\nNote: No reducer is required.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb1.text = "The Recommended fan is PP030"
                            self.lb2.text = "Note: No Reducer is required"
                        }else if(finalTotalPressure>=501){
                            let alert = UIAlertController(title: "High Static Pressure Loss", message: "Contact Us, ESCO Micro Pte. Ltd\nChangi Street 1 Singapore 486777\nTel. no.: +65 65426920\nEmail: mail@escoglobal.com", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.lb1.isHidden = false
                            self.lb2.isHidden = false
                            self.lb3.isHidden = false
                            self.lb1.text = "Note: Please Contact ESCO Micro Pte. Ltd"
                            self.lb2.text = "21 Changi South Street 1 Singapore 486777"
                            self.lb3.text = "+65 65420833 | +65 65426920 | mail@escoglobal.com"
                        }
                    }
                break
            default:
                break
            }
        }
    }
    @IBAction func btnClearAll(_ sender: Any) {
        self.tf250Reducer.text = ""
        self.tf305Reducer.text = ""
        self.tfDuctLength.text = ""
        self.tf90Elbow.text = ""
        self.tf45Elbow.text = ""
        self.txtPLCircular.text = ""
        self.txtPL90.text = ""
        self.txtPL45.text = ""
        self.txtPLDR.text = ""
        self.txtTotalPL.text = ""
        self.tfSelectDD.text = "Select Duct Diameter Here"
        self.lb1.text = ""
        self.lb2.text = ""
        self.lb3.text = ""
        self.lb4.text = ""
        self.lb5.text = ""
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
extension String {
    func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
}
extension Double {
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}
extension String  {
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.letters) == nil
        }
    }
}



